from numbers import Number

def add_numbers(x: Number,y: Number) -> Number:
    """
    _summary_

    Args:
        x (int): first number we want to add
        y (float): second number we want to add
    
    Returns:
        float: the outcome of the addition of the input x and y
    """
    return x + y

def multiply_numbers(x: Number,y: Number) -> Number:
    """
    _summary_

    Args:
        x (int): first number we want to multiply
        y (float): second number we want to multiply

    Returns:
        float: the outcome of multiplying the input of x and y
    """
    return x*y