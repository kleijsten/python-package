from setuptools import setup, find_packages

setup(
    name='workshoppackage',
    version='0.1.0',
    python_version='>=3.8',
    description='This is my first package',
    author='Koen Leijsten',
    author_mail='k.leijsten@cmotions.nl',
    packages=find_packages()
)